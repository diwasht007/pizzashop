<?php

namespace App\Http\Controllers\backEnd;

use App\Constants\ServerMessage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Exception;

class productController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $product =  new Product();
            $product->name = $request->name;
            $product->p_type = $request->type;
            $product->description = $request->description;
            $product->price = $request->price;
            $product->total_quantity = $request->total_quantity;
            $product->reamaining_quantity = $request->total_quantity;
            $product->save();

        }catch(Exception $e){
            return response()->json(['error'=>$e->getMessage()],ServerMessage::SERVER_ERROR);
        }
        return response()->json(['Data'=>$product,'Message'=>'Stored Sucessfully'],ServerMessage::SUCESS);
       


       }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
