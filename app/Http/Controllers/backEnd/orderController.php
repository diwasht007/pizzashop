<?php

namespace App\Http\Controllers\backEnd;

use App\Constants\ServerMessage;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;

class orderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
        try {
            $order = new Order();
            $order->order_number = mt_rand(100000, 999999);
            $order->product_id = $request->product_id;
            $product = Product::findorfail($request->product_id);
            $order->order_price = $product->price;
            $order->order_status = Order::PENDING;
            $order->save();

            $product = Product::findorfail($request->product_id);
            $product->reamaining_quantity = $product->reamaining_quantity - 1;

            $product->save();
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], ServerMessage::SERVER_ERROR);
        }
        return response()->json(['oder' => $order, 'message' => 'your order has been placed'], ServerMessage::SUCESS);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
