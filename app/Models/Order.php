<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    const PENDING = 'pending';
    const PAID = 'paid';
    const CANCELLED = 'cancelled';
    const RETURNED = 'returned';

   public function products(){
    return $this->belongsTo(Product::class,'product_id','id');
   }
}
