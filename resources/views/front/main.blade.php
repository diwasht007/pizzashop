@extends('front.master')
@section('content')
<section class="showcase-area" id="showcase">
    <div class="showcase-container">
        <h1 class="main-title">Eat Right Food</h1>
        <p>Eat healty food to be healty and fit</p>
        <a href="#food-menu" class="btn btn-primary">Menu</a>
    </div>
</section>



<section id="about">
    <div class="about-wrapper container">
        <div class="about-text">
            <p class="small">About Us</p>
            <h2>We have been making food for last 20 years for you</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum fugit voluptates neque illum voluptas doloribus, iste fuga laboriosam assumenda cupiditate quo quod repudiandae. Dolorem itaque vero distinctio quam harum eveniet!</p>
        </div>
        <div class="about-img">
            <img src="./images/about-photo.jpg" alt="About Us">
        </div>
    </div>
</section>

<section id="food">
    <h2>Please choose </h2>
    <div class="food-container container">
        <div class="food-type fruit">
            <div class="img-container">
                <img src="./images/vegPizza.webp" alt="">
                {{-- <img src="{{ asset('food1.jpg') }}" alt=""> --}}

                <div class="img-content">
                    <h3>Veg</h3>
                    <a href="https://en.wikipedia.org/wiki/Fruit" class="btn btn-primary" target="_blank">Click me</a>

                </div>
            </div>
        </div>
        <div class="food-type vegetable">
            <div class="img-container">
            <img src="./images/nonVegPizza.jpeg" alt="">
            <div class="img-content">
                <h3>Non-veg</h3>
                <a href="https://en.wikipedia.org/wiki/Vegetable" class="btn btn-primary" target="_blank">Click me</a>
            </div>
            </div>
        </div>
        {{-- <div class="food-type grain">
            <div class="img-container">

            <img src="./images/food3.jpg" alt="">
            <div class="img-content">
                <h3>Grain</h3>
                <a href="https://en.wikipedia.org/wiki/Grain" class="btn btn-primary" target="_blank">Learn More</a>

            </div>
            </div>
        </div> --}}
        </div>
    </div>
</section>

<section id="food-menu">
    <h2 class="food-menu-heading">
        Food Menu
    </h2>
    <div class="food-menu-container">
        <div class="food-menu-item">
            <div class="food-image">
                <img src="./images/food-menu1.jpg" alt="">
            </div>
            <div class="food-description">
                <h2 class="food-title">Food Menu Item 1</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, optio. Rem, iste, sequi expedita omnis autem numquam culpa placeat aspernatur sit iure quos temporibus magnam exercitationem iusto ex minus ut!</p>
                <p class="food-price">price: &#36; 10.99</p>
            </div>
        </div>
        <div class="food-menu-item">
            <div class="food-image">
                <img src="./images/food-menu2.jpg" alt="">
            </div>
            <div class="food-description">
                <h2 class="food-title">Food Menu Item 2</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, optio. Rem, iste, sequi expedita omnis autem numquam culpa placeat aspernatur sit iure quos temporibus magnam exercitationem iusto ex minus ut!</p>
                <p class="food-price">price: &#36; 10.99</p>
            </div>
        </div>   <div class="food-menu-item">
            <div class="food-image">
                <img src="./images/food-menu3.jpg" alt="">
            </div>
            <div class="food-description">
                <h2 class="food-title">Food Menu Item 3</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, optio. Rem, iste, sequi expedita omnis autem numquam culpa placeat aspernatur sit iure quos temporibus magnam exercitationem iusto ex minus ut!</p>
                <p class="food-price">price: &#36; 10.99</p>
            </div>
        </div>   <div class="food-menu-item">
            <div class="food-image">
                <img src="./images/food-menu4.jpg" alt="">
            </div>
            <div class="food-description">
                <h2 class="food-title">Food Menu Item 4</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, optio. Rem, iste, sequi expedita omnis autem numquam culpa placeat aspernatur sit iure quos temporibus magnam exercitationem iusto ex minus ut!</p>
                <p class="food-price">price: &#36; 10.99</p>
            </div>
        </div>   <div class="food-menu-item">
            <div class="food-image">
                <img src="./images/food-menu5.jpg" alt="">
            </div>
            <div class="food-description">
                <h2 class="food-title">Food Menu Item 5</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, optio. Rem, iste, sequi expedita omnis autem numquam culpa placeat aspernatur sit iure quos temporibus magnam exercitationem iusto ex minus ut!</p>
                <p class="food-price">price: &#36; 10.99</p>
            </div>
        </div>   <div class="food-menu-item">
            <div class="food-image">
                <img src="./images/food-menu6.jpg" alt="">
            </div>
            <div class="food-description">
                <h2 class="food-title">Food Menu Item 6</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, optio. Rem, iste, sequi expedita omnis autem numquam culpa placeat aspernatur sit iure quos temporibus magnam exercitationem iusto ex minus ut!</p>
                <p class="food-price">price: &#36; 10.99</p>
            </div>
        </div>

    </div>
</section>

<section id="testimonial">
    <h2 class="testimonial-title">What Our Customer Says</h2>
    <div class="testimonial-container container">
        <div class="testimonial-box">
            <div class="star-rating">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>


            </div>
            <p class="testimonial-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Expedita dolorum vitae voluptatem aliquid et autem praesentium qui iure necessitatibus ratione ut cum in, sunt at iusto accusantium voluptates ullam consectetur.</p>
            <div class="customer-detail">
                <div class="customer-photo">
                    <img src="./images/male1.jpeg" alt="">
                </div>
                <div class="customer-name">Jhon Doe</div>
            </div>
        </div>
        <div class="testimonial-box">
            <div class="star-rating">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span> 

            </div>
            <p class="testimonial-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Expedita dolorum vitae voluptatem aliquid et autem praesentium qui iure necessitatibus ratione ut cum in, sunt at iusto accusantium voluptates ullam consectetur.</p>
            <div class="customer-detail">
                <div class="customer-photo">
                    <img src="./images/male2.JPG" alt="">
                </div>
                <div class="customer-name">jay Doe</div>
            </div>
        </div><div class="testimonial-box">
            <div class="star-rating">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>

            </div>
            <p class="testimonial-text">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Expedita dolorum vitae voluptatem aliquid et autem praesentium qui iure necessitatibus ratione ut cum in, sunt at iusto accusantium voluptates ullam consectetur.</p>
            <div class="customer-detail">
                <div class="customer-photo">
                    <img src="./images/male3.png" alt="">
                </div>
                <div class="customer-name">Gunny Doe</div>
            </div>
        </div>
    </div>
</section>

<section id="contact">
    <div class="contact-container container">
        <div class="contact-image">
            <img src="./images/restraunt-image.jpg" alt="">
        </div>
        <div class="form-container">
            <h2>Contact Us</h2>
            <input type="text" name="" id="" placeholder="Name">
            <input type="email" name="" id="" placeholder="E-mail">
            <textarea name="" id="" cols="30" rows="10" placeholder="Type Your Message Here"></textarea>
            <a href="#" class="btn btn-primary">Submit</a>
        </div>
    </div>
</section>
    
@endsection



