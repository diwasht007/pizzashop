@extends('front.master')
@section('content')
    <!------ Include the above in your HEAD tag ---------->
    {{-- @if ($errors->any())
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    @endif --}}
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div> 
        <form class="form-horizontal" action="{{ route('product.store') }}" method='POST'>
            @csrf
            <fieldset>
                <!-- Form Name -->
                <legend>PRODUCTS</legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">PRODUCT NAME</label>
                    <div class="col-md-4">
                        <input id="product_name" name="name" placeholder="PRODUCT NAME" class="form-control input-md"
                            type="text">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">PRODUCT TYPE</label>
                    <div class="col-md-4">
                        <input id="type" name="type" placeholder="PRODUCT TYPE" class="form-control input-md"
                            type="text">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="description">PRODUCT PRICE</label>
                    <div class="col-md-4">
                        <input id="price" name="price" placeholder="price" class="form-control input-md"
                            type="text">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="total_quantity">AVAILABLE QUANTITY</label>
                    <div class="col-md-4">
                        <input id="total_quantity" name="total_quantity" placeholder="AVAILABLE QUANTITY"
                            class="form-control input-md" type="text">

                    </div>
                </div>
             

                <!-- Textarea -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="description">PRODUCT DESCRIPTION</label>
                    <div class="col-md-4">
                        <textarea class="form-control" id="description" name="description"></textarea>
                    </div>
                </div>



                <!-- Button -->
                <div class="form-group">
                    <div class="col-md-4">
                        <button id="add" name="add" class="btn btn-primary">Add Product</button>
                       
                    </div>

                </div>

            </fieldset>
        </form>


    </div>
@endsection
